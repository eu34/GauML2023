# matplotlib, scipy, scikit-learn

import pandas
from sklearn import linear_model


# df = pandas.read_csv("data.csv")

l = {
    "v1": [3, 4, 50, 6, 6],
    "v2": [4, 50, 6, 30, 7],
    "v3": [40, 77, 61, 32, 76]
}

df = pandas.DataFrame(l)
X = df[["v1", "v2"]]
# print(X)
y= df['v3']


# print(df)
# y = [3, 4, 6, 7, 9]

regr = linear_model.LinearRegression()
regr.fit(X, y)
print(regr.score(X, y))


#predict the CO2 emission of a car where the weight is 2300g, and the volume is 1300ccm:
predictedCO2 = regr.predict([[7, 3]])
#
print(predictedCO2)


# from scipy import stats
#
# x = [5,7,8,7,2,17,2,9,4,11,12,9,6]
# y = [99,86,87,88,111,86,103,87,94,78,77,85,86]
#
# s, i, r, x1, x2 = stats.linregress(x, y)
#
# def myfunc(x):
#   return s * x + i
#
# _y = []
#
# for a in x:
#     _y.append(myfunc(a))

# mymodel = list(map(myfunc, x))


# print(list(map(myfunc, x)))
# print(_y)

# print(s*13+i)

# print(r)

