import numpy as np

n1 = np.array([2, 3, 4, 5])
# print(n1)
n2 = np.array([[2, 4, 5, 6, 7],
               [8, 7, 9, 7, 8],
               [32, 4, 5, 5, 6],
               [2, 14, 15, 52, 61]])
# print(n2)
# print(n2[:2:3,1:4])

n3 = np.zeros((3, 4), dtype=int)
# print(n3)
n4 = np.random.randint(5, 20, size=(4, 5))
# print(n4)
# print(n2[1][1])
# print(n2[1,1])
# print(n2[:3][1:4])

# n2[::2,1:4] = 1
# print(n2)

n = np.random.randint(3, 15, size=(4, 7))
# print(n)
# print(n[::3, ::2])
n = np.random.randint(3, 20, size=(4, 6, 2))
print(n)
print(n.shape)
n1 = n.reshape(-1, 8)
print(n1)
n2 = n1.reshape(3, -1)
print(n2)


