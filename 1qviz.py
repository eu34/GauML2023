def allDivisors(num):
    results = set([])

    for i in range(2, num):
        if num % i == 0:
            results.add(i)

    return results


for i in range(11, 1001, 11):
    divisors = allDivisors(i)
    if len(divisors) == 2 and 11 in divisors:
        print(i)


