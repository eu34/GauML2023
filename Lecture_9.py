from scrapy.http import TextResponse
import requests
import pprint
import pandas

url = requests.get("https://place.ge/ge/ads?object_type=flat&mode=list&nearest=0&type=for_sale&condition=&project=&agency_id=&city_id=1&region_id=&district_id=&street_id=&commercial_type=&commercial_type2=&status=&rooms_from=-%E1%83%93%E1%83%90%E1%83%9C&rooms_to=-%E1%83%9B%E1%83%93%E1%83%94&living_space_from=-%E1%83%93%E1%83%90%E1%83%9C&living_space_to=-%E1%83%9B%E1%83%93%E1%83%94&price_from=-%E1%83%93%E1%83%90%E1%83%9C&price_to=-%E1%83%9B%E1%83%93%E1%83%94&currency_id=1&with_photos=0&owner=0")

res = TextResponse(url.url, body=url.text, encoding='utf-8')
# print(url)
# pprint.pprint(res.text)

urls = res.css("div.img a::attr(href)").getall()
# pprint.pprint(urls)

for u in urls:
    if u!='/ge/faq?open=5':
        print("https://place.ge"+u)